import React from 'react'
import { View, Text, ImageBackground } from 'react-native'
import styles from './styles'

export default function index() {
    return (
        <View>
            <ImageBackground
                source={require('../../assets/images/fotogw.jpg')}
                style={styles.image} />
        </View>
    )
}
