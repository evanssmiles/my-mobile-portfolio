import React from 'react'
import { View, StyleSheet, StatusBar } from 'react-native'
import MainMenu from './src/components/mainMenu'

export default function App() {
  return (
    <View style={styles.maincontainer}>
      <MainMenu />
      <StatusBar barStyle="default" />
    </View>
  )
}

const styles = StyleSheet.create({
  maincontainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'pink'

  }
})
